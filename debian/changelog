ser-player (1.7.3-3) unstable; urgency=medium

  * Team upload.
  * Add 'd/clean' and add missing elements. (Closes: #1048498)
  * 'd/control':
    - Use 'debhelper-compat' 13.
    - Update 'Standards-Version' to 4.7.0, no changes required.
    - Change 'Homepage' to GitHub repository as previous no longer exists.
  * 'd/copyright':
    - Add missing 'Libpng' licensed file 'src/pnglibconf.h'.
    - Create 'Files: debian/*' section and add appropriate persons.
  * 'd/watch': Update to 'version' 4.

 -- Phil Wyett <philip.wyett@kathenas.org>  Thu, 10 Oct 2024 17:21:41 +0100

ser-player (1.7.3-2) unstable; urgency=medium

  * Team upload.
  * Convert patch to gbp format
  * Add missing QPainterPath include (Closes: #975145)

 -- Ole Streicher <olebole@debian.org>  Tue, 05 Jan 2021 16:08:48 +0100

ser-player (1.7.3-1) unstable; urgency=medium

  * New upstream version

 -- Cyril Richard <lock042@gmail.com>  Tue, 04 Feb 2020 09:32:28 +0100

ser-player (1.7.2-3) unstable; urgency=medium

  * Fix patches. One patch was reverting the first patch.

 -- Cyril Richard <lock042@gmail.com>  Wed, 07 Mar 2018 13:49:43 +0100

ser-player (1.7.2-2) unstable; urgency=medium

  * Fix a bug in version numbering. Allow the app version to be set directly.

 -- Cyril Richard <lock042@gmail.com>  Wed, 14 Feb 2018 15:06:42 +0100

ser-player (1.7.2-1) unstable; urgency=medium

  * New upstream version

 -- Cyril Richard <lock042@gmail.com>  Tue, 13 Feb 2018 12:00:14 +0100

ser-player (1.7.1-1) unstable; urgency=medium

  * New upstream version

 -- Cyril Richard <lock042@gmail.com>  Thu, 07 Dec 2017 20:32:52 +0100

ser-player (1.7.0-5) unstable; urgency=medium

  * Fixes spelling-error in manpage
  * Fixes metainfo xml data
  * Fixes various other minor lintian outputs in rules, control, .desktop files

 -- Cyril Richard <lock042@gmail.com>  Fri, 27 Oct 2017 15:38:14 +0200

ser-player (1.7.0-4) unstable; urgency=medium

  * New hurd patch to fix build on hurd-i386 platform. Closes: #879710

 -- Cyril Richard <lock042@gmail.com>  Wed, 25 Oct 2017 07:59:10 +0200

ser-player (1.7.0-3) unstable; urgency=medium

  * Updates kFreeBsD patch to fix build on all kFreeBSD platforms. Closes: #879126

 -- Cyril Richard <lock042@gmail.com>  Tue, 24 Oct 2017 06:37:07 +0200

ser-player (1.7.0-2) unstable; urgency=medium

  * Adding patch to fix build fails on kFreeBSD. Closes: #879126

 -- Cyril Richard <lock042@gmail.com>  Sat, 21 Oct 2017 14:43:41 +0200

ser-player (1.7.0-1) unstable; urgency=medium

  * New package. Closes: #876830

 -- Cyril Richard <lock042@gmail.com>  Fri, 13 Oct 2017 14:25:07 +0200
